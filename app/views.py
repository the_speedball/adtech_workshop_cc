import uuid
import datetime

from tinydb import TinyDB, Query
from urlparse import parse_qs


from flask import render_template, make_response, request, jsonify
from app import app


@app.route('/ad')
def ad():
    uid = request.cookies.get('uid')
    print(uid)
    db = TinyDB('data.json')
    user = Query()

    user_products = db.search(user.uid == uid)
    print(user_products)
    product_count = {}
    for pro in user_products:
        if pro['sku'] not in product_count:
            product_count[pro['sku']] = 0
        product_count[pro['sku']] += 1

    min = 0
    name = 'FEDORA'

    print(product_count)
    for sku, amount in product_count.items():
        if amount > min:
            print(sku, amount)
            min = amount
            name = sku

    return '''
    <div style="position:relative;">
    <a href="http://shop.workshop.clearcode.cc/product_sku/{sku}" target="_parent" style="display: block;">
    <div style="background-image: url('http://adserver.workshop.clearcode.cc/media/creatives/{sku}.png'); width: 92px; height: 105px;background-position: center;background-size: contain;background-repeat: no-repeat;position: absolute;top: 68px;left: 41px;"/></div>
    <div style="position: absolute;top: 198px;left: 31px;font-family: helvetica;width: 112px;height:40px; text-align:center;">
      <span style="font-weight: bold; font-size: 11px; text-align: center;position: absolute;  top: 50%;  transform:translateY(-50%); width:100%; left:0; text-decoration:none !important; color:black;">{ad_text}
      </span>
    </div>
    <img src="http://adserver.workshop.clearcode.cc/media/creatives/300x250_base.png"/>
  </a>
</div>'''.format(sku=name.replace("'", ''), ad_text='elo')


@app.route('/product')
def product():

    uid = request.cookies.get('uid')
    if not uid:
        print('New UID')
        uid = str(uuid.uuid4())
    print(uid)

    rsp = make_response()
    rsp.set_cookie('uid', value=uid)

    db = TinyDB('data.json')

    qs = parse_qs(request.query_string)

    clean = {k: v[0] for k, v in qs.items()}

    if 'po' in clean:
        products = []
        clean['po'] = clean['po'][1:-1]
        for p in clean['po'].split('_'):
            sku, pcc, amount = p.split(',')
            products.append({'sku': sku,
                             'amount': amount,
                             'pcc': pcc})
        clean['products'] = products
        del clean['po']

    del clean['cachebuster']
    clean['uid'] = uid
    clean['timestamp'] = int(datetime.datetime.utcnow().strftime('%s'))
    db.insert(clean)

    print(db)
    print(request.query_string)

    return rsp


@app.route('/report/<string:uid>')
def report(uid):
    db = TinyDB('data.json')
    user = Query()
    return jsonify(db.search(user.uid == uid))
